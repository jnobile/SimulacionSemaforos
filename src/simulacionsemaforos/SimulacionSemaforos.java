/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacionsemaforos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import javax.swing.JFrame;


/**
 *
 * @author juliana
 */
public class SimulacionSemaforos {

    /**
     * @param args the command line arguments
     */
    
    int tiempo_fin ;
    int reloj;
    boolean colision = false;

    
    // tiempos fases
    int t_faseRojoCompartida;
    int t_faseRojoIzq;
    int t_faseRojoDer;
    int t_faseVerdeIzq;
    int t_faseVerdeDer;
    int t_subida_puente;
    
    
    ArrayList<Object[]> LEF; /// LEF Semaforos y carros
    ArrayList<Object[]> historial_s;
    ArrayList<Integer> carrosPuente ;
    ArrayList<Integer> [] tabla_I = new ArrayList[5];
    ArrayList<Integer> [] tabla_D = new ArrayList[5];
    
    //ArrayList<Object[]> historial_c;
    
    // Eventos
    static final String CAMBIA_SEMAFORO = "Cambia fase semaforo";
    static final String Llegada_I = "Llega Carro Izquierda";
    static final String Llegada_D = "Llega Carro Derecha";
    static final String Salida_I = "Sale Carro Izquierda";
    static final String Salida_D = "Sale Carro Derecha";
    static final String Subida_I = "Sube carro Izquierda";
    static final String Subida_D = "Sube carro Derecha";
    
    //  Colas
    int colaIzq = 0; 
    int colaDer = 0;
    
    
    private Aleatorio random;
    // 1. Semaforo Izquierdo rojo - Semaforo Derecho rojo
    // 2. Semaforo Izquierdo rojo - Semaforo Derecho verde
    // 3. Semaforo Izquierdo rojo - Semaforo Derecho rojo
    // 4. Semaforo Izquierdo Verde - Semaforo Derecho rojo
    // Vuelve al paso 1 y repite fases.
    private int faseSemaforosSiguiente;
    private int faseSemaforosActual;
    private int relojSemaforoRojo;
    
    
    
    public SimulacionSemaforos() {
        // No se usa, se usa el que esta en VentanaPrincipal.Java
        contructSimulacionSemaforos(50, 100, 70, 600);
    }
    
    public SimulacionSemaforos(int faseRojoCompartida, int faseVerdeIzq, int faseVerdeDer, int tiempoEjecucion ) {        
        contructSimulacionSemaforos(faseRojoCompartida, faseVerdeIzq, faseVerdeDer, tiempoEjecucion);
    }
    
    public void contructSimulacionSemaforos(int faseRojoCompartida, int faseVerdeIzq, int faseVerdeDer, int tiempoEjecucion ) {
        
        this.t_faseRojoCompartida = faseRojoCompartida;
        this.t_faseVerdeIzq = faseVerdeIzq;
        this.t_faseVerdeDer = faseVerdeDer;
        this.tiempo_fin = tiempoEjecucion;
        
        inicializacion();
        iniciarSimulacion();
    }
    
    private void inicializacion() {
        
        tabla_I[0]= new ArrayList<Integer>(); // Llegadas Izq
        tabla_I[1]= new ArrayList<Integer>(); // Subidas Izq
        tabla_I[2]= new ArrayList<Integer>(); // Salidas Izq
        tabla_I[3]= new ArrayList<Integer>(); // Cola Izq
        tabla_I[4]= new ArrayList<Integer>(); // Espera = Subida - Llegada
        
        tabla_D[0]= new ArrayList<Integer>(); // Llegadas Izq
        tabla_D[1]= new ArrayList<Integer>(); // Subidas Izq
        tabla_D[2]= new ArrayList<Integer>(); // Salidas Izq
        tabla_D[3]= new ArrayList<Integer>(); // Cola Izq
        tabla_D[4]= new ArrayList<Integer>(); // Espera = Subida - Llegada
        
        reloj = 0;
        relojSemaforoRojo = 0;
        
        t_faseRojoDer = t_faseVerdeIzq + (2*t_faseRojoCompartida);
        t_faseRojoDer = t_faseVerdeDer + (2*t_faseRojoCompartida);
        t_subida_puente = 5;
        
        // Estado actual semaforos rojo, siguiente -> cambia semaforo derecho a verde
        System.out.println("Izq Rojo - Der Rojo");
        faseSemaforosActual = 4;
        faseSemaforosSiguiente = 1;
        
        random = new Aleatorio();
        LEF = new ArrayList<>();
        historial_s = new ArrayList<>();
        carrosPuente = new ArrayList<>();
        
        //int llegadaI = random.getRamdomIzq();
        //int llegadaD = random.getRamdomDer();
        
        // Se inicia con 5 eventos futuros
        // 1 proximo evento semaforo
        // 2 y 3 proxima llegada de carros izq y der 
        // 4 y 5 proxima salida de carro izq y der
        addEvent(new Object[]{CAMBIA_SEMAFORO,0} );
        addEvent(new Object[]{Llegada_I,0});
        addEvent(new Object[]{Llegada_D,0});
        
        //tabla_D[0].add(0);
        //tabla_I[0].add(0);
        //addEvent(new Object[]{Salida_I,llegadaI+t_faseRojoIzq+t_subida_puente+random.getRamdomPaso()});
        //addEvent(new Object[]{Salida_D,llegadaD+t_faseRojoCompartida+t_subida_puente+random.getRamdomPaso()});
    }
    
    public void iniciarSimulacion(){
        

        //estadoLEF();
        try {
            while(reloj<tiempo_fin){
                estadoLEF();
                executeEvent(); // Devuelve el proximo evento
            }
        } catch (Exception e){
            System.out.println("Colision en "+e.getMessage());
            colision = true;
            //e.printStackTrace();
        }
        estadoTabla_D();
        estadoTabla_I();
        System.out.println("Reloj "+reloj);
        
        for (Object[] registro : historial_s) {
            System.out.println("Event (t="+registro[1]+"):"+registro[0]);
        }
        
        
    }

    public void addEvent(Object[] event){

        LEF.add(event); // Agrega un evento al LEF_S 
        LEF.sort(sortLEF); // Ordena el left por el tiempo del reloj        
    }
    
    public void executeEvent() throws Exception{
        
        String evento = (String)LEF.get(0)[0];
        reloj = (int)LEF.get(0)[1];
        
        // removemos el evento que estamos ejecutando
        for (Object[] obj : LEF) {
            if(obj[0]==evento){
                LEF.remove(obj);
                break;
            }
        }
        
        switch (evento){
            
            case CAMBIA_SEMAFORO:
                System.out.println("Carros puente: "+carrosPuente.size());
                switch (faseSemaforosSiguiente){
                    
                        // 1. Semaforo Izquierdo rojo - Semaforo Derecho rojo
                        // 2. Semaforo Izquierdo rojo - Semaforo Derecho verde
                        // 3. Semaforo Izquierdo rojo - Semaforo Derecho rojo
                        // 4. Semaforo Izquierdo Verde - Semaforo Derecho rojo
                    case 1: 
                        System.out.println("Rojo los 2");
                        addEvent(new Object[]{CAMBIA_SEMAFORO,reloj + t_faseRojoCompartida} );
                        faseSemaforosActual = faseSemaforosSiguiente;
                        faseSemaforosSiguiente = 2;
                        
                    break;
                    case 2: 
                        System.out.println("Der Verde");
                        if(carrosPuente.size()>0){
                            estadoTabla_D();
                            estadoTabla_I();
                            throw new Exception("Case 2");
                        }
                        relojSemaforoRojo = reloj + t_faseVerdeDer;
                        addEvent(new Object[]{CAMBIA_SEMAFORO,reloj + t_faseVerdeDer} );
                        addEvent(new Object[]{Subida_D,reloj + t_subida_puente} );
                        faseSemaforosActual = faseSemaforosSiguiente;
                        faseSemaforosSiguiente = 3;
                        
                        
                    break;
                    case 3: 
                        System.out.println("Rojo los 2");
                        addEvent(new Object[]{CAMBIA_SEMAFORO,reloj + t_faseRojoCompartida} );
                        faseSemaforosActual = faseSemaforosSiguiente;
                        faseSemaforosSiguiente = 4;
                        
                    break;
                    case 4: 
                        System.out.println("Izq Verde");
                        if(carrosPuente.size()>0){
                            estadoTabla_D();
                            estadoTabla_I();
                            throw new Exception("Case 4");
                        }
                        relojSemaforoRojo = reloj + t_faseVerdeIzq;
                        addEvent(new Object[]{CAMBIA_SEMAFORO,reloj + t_faseVerdeIzq} );
                        addEvent(new Object[]{Subida_I,reloj + t_subida_puente} );
                        faseSemaforosActual = faseSemaforosSiguiente;
                        faseSemaforosSiguiente = 1;
                        
                    break;
                }
                System.out.println("Fase: "+faseSemaforosActual);
                guardar(historial_s, faseSemaforosActual, reloj);
            break;
            case Llegada_I: // crean llegadas y la primer subida
                System.out.println("L:Carros puente I: "+carrosPuente.size()+" - Cola:"+colaIzq);
                tabla_I[0].add(reloj);
                addEvent(new Object[]{Llegada_I,reloj + random.getRamdomIzq()} );
                if(faseSemaforosActual == 4 && colaIzq==0){ // es el primero en llegar
                    addEvent(new Object[]{Subida_I,reloj + t_subida_puente} );
                } 
                colaIzq++;
                tabla_I[3].add(colaIzq);
                System.out.println("L:Carros puente I: "+carrosPuente.size()+" - Cola:"+colaIzq);
                estadoTabla_I();
            break;
            case Llegada_D: // crean llegadas y la primer subida
                System.out.println("L:Carros puente D: "+carrosPuente.size()+" - Cola:"+colaDer);
                tabla_D[0].add(reloj);
                addEvent(new Object[]{Llegada_D,reloj + random.getRamdomDer()} );
                
                if(faseSemaforosActual == 2 && colaDer==0){ // si semaforo en verde y es el primero, entonces.. generar subida
                    addEvent(new Object[]{Subida_D,reloj + t_subida_puente} );
                } 
                colaDer++;
                tabla_D[3].add(colaDer);
                System.out.println("L:Carros puente D: "+carrosPuente.size()+" - Cola:"+colaDer);
                estadoTabla_D();
            break;
            case Subida_I: 
                System.out.println("E:Carros puente I: "+carrosPuente.size()+" - Cola:"+colaIzq);
                colaIzq--; // siempre cola-- porque se esta ejecutando una subida 
                // Agregamos a la tabla los datos de subida y el de espera
                tabla_I[1].add(reloj);
                int llegada = (tabla_I[1].size()-1)>=tabla_I[0].size()?-1000:tabla_I[0].get(tabla_I[1].size()-1);
                tabla_I[4].add(reloj - llegada); // Espera = subida - Llegada
                
                // Verificamos si el semaforo esta en verde y nos alcanza el tiempo para generar la proxima subida
                if(faseSemaforosActual == 4 && reloj + t_subida_puente<relojSemaforoRojo){ // Si el semaforo en verde, entonces vamos a sacar otro carro
                    
                    // Si hay carros esperando por subir, y el puente no esta lleno, subimos el siguiente carro
                    if (colaIzq>0 && carrosPuente.size()<30){// si todavia hay cola
                        addEvent(new Object[]{Subida_I,reloj + t_subida_puente });  // agregamos al LEF la proxima subida          
                    } 
                }
                // si somos la primer subida, creamos nuestra salida
                if (!(carrosPuente.size()>0)){ // si soy el primero en subir, genero mi salida
                    addEvent(new Object[]{Salida_I,reloj + random.getRamdomPaso() });
                }
                carrosPuente.add(reloj); // se agrega el carro que esta pasando, osea este evento actual. Se agrega despues de verificar para la primer salida
                
                System.out.println("E:Carros puente I: "+carrosPuente.size()+" - Cola:"+colaIzq);
                estadoTabla_I();
            break; 
            case Subida_D: 
                System.out.println("E:Carros puente I: "+carrosPuente.size()+" - Cola:"+colaIzq);
                colaDer--; // siempre cola-- porque se esta ejecutando una subida 
                // Agregamos a la tabla los datos de subida y el de espera
                tabla_D[1].add(reloj);
                int llegada_d = (tabla_D[1].size()-1)>=tabla_D[0].size()?-1000:tabla_D[0].get(tabla_D[1].size()-1);
                tabla_D[4].add(reloj - llegada_d); // Espera = subida - Llegada
                
                // Verificamos si el semaforo esta en verde y nos alcanza el tiempo para generar la proxima subida
                if(faseSemaforosActual == 2 && reloj + t_subida_puente<relojSemaforoRojo){ // Si el semaforo en verde, entonces vamos a sacar otro carro
                    
                    // Si hay carros esperando por subir, y el puente no esta lleno, subimos el siguiente carro
                    if (colaDer>0 && carrosPuente.size()<30){// si todavia hay cola
                        addEvent(new Object[]{Subida_D,reloj + t_subida_puente });  // agregamos al LEF la proxima subida          
                    } 
                }
                // si somos la primer subida, creamos nuestra salida
                if (!(carrosPuente.size()>0)){ // si soy el primero en subir, genero mi salida
                    addEvent(new Object[]{Salida_D,reloj + random.getRamdomPaso() });
                }
                carrosPuente.add(reloj); // se agrega el carro que esta pasando, osea este evento actual. Se agrega despues de verificar para la primer salida
                System.out.println("E:Carros puente D: "+carrosPuente.size()+" - Cola:"+colaDer);
                estadoTabla_I();
            break; 
            case Salida_I:
                System.out.println("S:Carros puente I: "+carrosPuente.size()+" - Cola:"+colaIzq);
                tabla_I[2].add(reloj);
                int tiempoSubida = carrosPuente.get(0);
                carrosPuente.remove(0);
                if(carrosPuente.size()>0){
                    int tiempoSalida = tiempoSubida + random.getRamdomPaso();
                    tiempoSalida = tiempoSalida>reloj?tiempoSalida:reloj;
                    addEvent(new Object[]{Salida_I,tiempoSalida });
                }else if(colaIzq>0 && faseSemaforosActual == 4 && reloj + t_subida_puente<relojSemaforoRojo) {
                    // Se crea subida cuando hay carros en cola y no hay nada en el puente y el semaforo sigue en verde
                    addEvent(new Object[]{Subida_I,reloj + t_subida_puente });
                }
                System.out.println("S:Carros puente I: "+carrosPuente.size()+" - Cola:"+colaIzq);
                estadoTabla_I();
            break;            
            case Salida_D:
                System.out.println("S:Carros puente D: "+carrosPuente.size()+" - Cola:"+colaDer);
                tabla_D[2].add(reloj);
                int tiempoSubida_d = carrosPuente.get(0);
                carrosPuente.remove(0);
                if(carrosPuente.size()>0){
                    int tiempoSalida = tiempoSubida_d + random.getRamdomPaso();
                    tiempoSalida = tiempoSalida>reloj?tiempoSalida:reloj;
                    addEvent(new Object[]{Salida_D,tiempoSalida });
                }else if(colaDer>0 && faseSemaforosActual == 2 && reloj + t_subida_puente<relojSemaforoRojo) {
                    // Se crea subida cuando hay carros en cola y no hay nada en el puente y el semaforo sigue en verde
                    addEvent(new Object[]{Subida_D,reloj + t_subida_puente });
                }
                System.out.println("S:Carros puente D: "+carrosPuente.size()+" - Cola:"+colaDer);
                estadoTabla_D();
            break;
            
        }
        
    }
    
    private void estadoLEF(){
        
        for (Object[] l : LEF) {
            System.out.println("LEF: "+l[0] + ": \t "+ l[1]);
        }
        System.out.println("");
    }
    
    private String estadoTabla_I(){
        
        String estado = "";
        
        estado += "Tabla Carros Izquierda\n";
        estado += "Llegadas:  \n";
        for (Integer n : tabla_I[0]) {
            estado += n+"\t";
        }estado += "\n";
        
        estado += "Subidas: \n";
        for (Integer n : tabla_I[1]) {
            estado += n+"\t";
        }estado += "\n";
        
        estado += "Salidas: \n";
        for (Integer n : tabla_I[2]) {
            estado += n+"\t";
        }estado += "\n";
        
        estado += "Cola:      \n";
        for (Integer n : tabla_I[3]) {
            estado += n+"\t";
        }estado += "\n";
        
        estado += "Esperas: \n";
        for (Integer n : tabla_I[4]) {
            estado += n+"\t";
        }estado += "\n";

        estado += "\n";
        System.out.println(estado);
        return estado;
    }
    
    private String estadoTabla_D(){
        
        String estado = "";
                
        estado += "Tabla Carros Derecha \n";
        
        estado += "Llegadas:  \n";
        for (Integer n : tabla_D[0]) {
            estado += n+"\t";
        }estado += "\n";
        
        estado += "Subidas: \n";
        for (Integer n : tabla_D[1]) {
            estado += n+"\t";
        }estado += "\n";
        
        estado += "Salidas: \n";
        for (Integer n : tabla_D[2]) {
            estado += n+"\t";
        }estado += "\n";
        
        estado += "Cola:      \n";
        for (Integer n : tabla_D[3]) {
            estado += n+"\t";
        }estado += "\n";
        
        estado += "Esperas: \n";
        for (Integer n : tabla_D[4]) {
            estado += n+"\t";
        }estado += "\n";

        estado += "\n";
        System.out.println(estado);
        return estado;
    }
    
    public void guardar(ArrayList<Object[]> historial, int fase, int reloj){
        Object[] registro = {fase, reloj};
        historial.add(registro);
    }
    
    public Comparator<Object[]> sortLEF = new Comparator<Object[]>() {
 
            @Override
	    public int compare(Object[] event1, Object[] event2) {
 
	      int tiempo1 = (int)event1[1];
	      int tiempo2 = (int)event2[1];
              
	      return tiempo1 - tiempo2;
 
	    }
 
	};
    
    public Comparator<Object[]> sortDESC= new Comparator<Object[]>() {
 
            @Override
	    public int compare(Object[] event1, Object[] event2) {
 
	      int tiempo1 = (int)event1[1];
	      int tiempo2 = (int)event2[1];
              
	      return tiempo2 - tiempo1;
 
	    }
 
	};

    String getresultado() {
        String resultado = "";
        
        resultado += estadoTabla_I();
        resultado += estadoTabla_D();
        return resultado;
    }

    
}
