/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacionsemaforos;

import java.security.SecureRandom;
import java.util.ArrayList;
import org.omg.CORBA.MARSHAL;

/**
 *
 * @author juliana
 */
public class Aleatorio {

    SecureRandom random = new SecureRandom();
    ArrayList aleatorios;
    
    public Aleatorio() {
        this.aleatorios = new ArrayList();
    }
    
    Integer getRamdomPaso (){

        double randomd = random.nextDouble();
        int aleatorioUni = new Double(65+(randomd*11)).intValue();
        //System.out.println(aleatorioUni);
        return aleatorioUni;
    }
    
    Integer getRamdomIzq (){
        return getRamdom (0.031810766721044);
    }
    
    Integer getRamdomDer (){
        return getRamdom (0.045822102425876);
    }
    
    Integer getRamdom (double l){
        double randomd = random.nextDouble();
        int aleatorioExp = new Double((-(1/l)*Math.log(randomd))).intValue();
        return aleatorioExp;
    }
    
    ArrayList<Integer> getRamdoms (int cantidad, double l){
        
        for (int i = 0; i < cantidad; i++) {
            double randomd = random.nextDouble();
            //System.out.println(randomd);
            int aleatorioExp = new Double((-(1/l)*Math.log(randomd))).intValue();
            aleatorios.add(aleatorioExp);
        }
        return aleatorios;
    }
    
    
    
}
